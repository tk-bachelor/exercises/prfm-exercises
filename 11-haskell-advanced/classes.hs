-- Ex 1.
data Nat = Zero | Succ Nat deriving Show

add :: Nat -> Nat -> Nat
add Zero n = n
add (Succ m) n = Succ (add m n)

mul :: Nat -> Nat -> Nat
mul m Zero = Zero
mul m (Succ n) = add m (mul m n)

-- Ex 4.

data Tree a = Leaf a | Node (Tree a) (Tree a) deriving Show

halve xs = splitAt (length xs `div` 2) xs

balance [x] = Leaf x
balance xs = Node (balance ys) (balance zs)
            where (ys, zs) = halve xs

-- Ex 5.
data Expr = Val Int | Add Expr Expr deriving Show

folde :: (Int -> a) -> (a -> a -> a) -> Expr -> a
folde f g (Val n) = f n
folde f g (Add x y) = g (folde f g x) (folde f g y)

-- Ex 6.
eval :: Expr -> Int
eval = folde id (+)

size = folde (const 1) (+)