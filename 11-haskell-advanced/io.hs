-- Ex 1.
putStr2 xs = sequence_ [ putChar x | x <- xs]

-- Ex 4.

adder = do  putStr "How many numbers? "
            n <- getLine
            adder2 0 (read n)

adder2 total 0 = do putStr "The total is "
                    putStrLn (show total)
adder2 total n = do num <- getLine
                    adder2 (total + read num) (n-1)