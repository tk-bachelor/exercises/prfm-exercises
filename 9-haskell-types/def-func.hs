-- Exercise 2.1
halve xs = (take n xs, drop n xs)
            where n = length xs `div` 2

halve2 xs = splitAt (length xs `div` 2) xs

-- Exercise 2.2
-- third xs = head (tail (tail xs))
-- third xs = xs !! 2
third (_:_:x:_) = x

-- Exercise 2.3

-- safetail xs = if null xs 
--               then [] 
--               else tail xs

-- safetail xs | null xs = []
--             | otherwise = tail xs

safetail [] = []
safetail (_:xs) = xs

-- Exercise 2.5
-- a && b = if a then
--             if b then True else False
--          else
--             False

-- a && b = if a then b else False

mult = \x -> (\y -> (\z -> x*y*z))

-- Exercise 2.8
luhnDouble d = if n > 9
                then n - 9
                else n
                where n = d * 2

luhn a b c d = (a' + b + c' + d) `mod` 10 == 0
                where 
                    a' = luhnDouble a
                    c' = luhnDouble c

