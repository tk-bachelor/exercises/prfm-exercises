# Prolog Testat - Study Plan
by `Felix Varghese Enchiparamban` & `Keerthikan Thurairatnam`

A module can have prerequisite modules and minimum required credits so that you can enroll for the next semester. Our program checks if you have fulfilled the requirements for each modules and suggest which modules you can enroll next semester.

## Knowledge Base
```java
module(oop).
module(an1).
module(bsys).
module(parprog).
module(se1).
module(se2).
module(an2).
module(sa).

ects(oop, 6).
ects(an1, 4).
ects(bsys, 4).
ects(parprog, 4).
ects(se1, 4).
ects(se2, 4).
ects(an2, 4).
ects(sa, 8).

% module X requires a list of modules as prerequisites and minimum ECTS modules
require(oop, [], 0).
require(an1, [], 0).
require(bsys, [], 0).
require(se1, [oop], 0).
require(se2, [se1], 0).
require(an2, [an1], 0).
require(parprog, [oop, bsys], 0).
require(sa, [se2, bsys, an1], 24). % SA requires min. 24 ECTS

% Question 1: find all required modules (direct and indirect dependencies) by mutual recursion
% Query: dependent(se2, X)
% Result: X = [se1, oop]
dependent(Module,Dep):- module(Module), require(Module, DirectDep, _), findAllDep(DirectDep,Dep).

findAllDep([],[]).
findAllDep([H|T], Dep) :- dependent(H,HDep), findAllDep(T,TDep), append([H|HDep], TDep, Dep).

% passed modules as list
% passed([]).
passed([oop, an1, bsys]).
not_passed(Module) :- module(Module), passed(Passed), not_member(Module, Passed).

% passed modules as single predicates
% passed(null).
% passed(oop).
% not_passed(Module) :- module(Module), passed(Passed), Passed \= null, Module \= Passed.

not_member(_, []).
not_member(X, [H|T]) :- X \= H, not_member(X, T).

% Question 2: Which modules can I enroll next?
% Query: canEnroll(X).
% Result: X = [parprog, se1, an2]
% canEnroll(Module) :- passed(null), require(Module, []).
canEnroll(Module) :- achievedEcts(AE), canEnroll(Module, AE).
canEnroll(Module, AE) :- module(Module), not_passed(Module), 
    require(Module, DirectDep, E), isEnough(AE, E),
    passed(Passed), subset(DirectDep, Passed).
    
% Question 3: How many ECTS Points have I already achieved?
% Query: achievedEcts(X)
% Result: 14
achievedEcts(X) :- passed(Passed), achievedEcts(Passed, X).
achievedEcts([], 0).
achievedEcts([M|T], E) :- achievedEcts(T, TE), ects(M, ME), E is TE + ME.

isEnough(Achieved, RequiredEcts) :- Achieved >= RequiredEcts.
```

## Example Queries

### 1. Dependent Modules

We would find out all the required moduels (direct and indirect dependencies)

```
?- dependent(se2, X).
X = [se1, oop].
```
SE2 is directly dependent on SE1 and SE1 is directly dependent on OOP. So the query returns SE1 and OOP as result.

### 2. Semester plan

Suppose that we have already passed the following modules,
```java
passed([oop, an1, bsys]).
```
we would like to find out which modules we could enroll for next semester.

```java
?- canEnroll(X).
X = parprog
X = se1
X = an2
```

The next possible modules are ParProg, SE1 and AN2. The reason for that is ParProg requires OOP and Bsys as prerequisites which are fulfilled. SE1 requires OOP and AN2 requires AN1. They are passed as well.

### 3. Achieved ECTS Points

We would like to know our achieved ECTS points.

```
?- achievedEcts(X).
X = 14.
```

OOP has 6 points. AN1 and Bsys both have 4 points each. That sums up to 14 ECTS points.

### Semester Plan including ECTS Points

Now we add SE1 and SE2 as passed modules to the current knowledge base. 
```
passed([oop, an1, bsys, se1, se2]).
```

We query once again which modules we can enroll for next semester.

```
?- canEnroll(X).
X = parprog
X = an2
```

`Remark`: It is important to note that SA module doesn't show up in the result even though we have passed the prerequisite modules. The reason is that SA requires 24 ECTS points, but we have only achieved 20 points.

So we add the module `AN2` as passed in order to fulfill the requirements of 24 ECTS points.
```
passed([oop, an1, bsys, se1, se2, an2]).
```

Then we run the query again.
```
?- canEnroll(X).
X = parprog
X = sa
```
As expected, the module `SA` shows up in the result.


