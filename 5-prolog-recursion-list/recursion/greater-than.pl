greater_than(succ(_), 0).
greater_than(succ(X), succ(Y)) :- greater_than(X, Y).

% greater_than(succ(succ(succ(0))), succ(0)).
% X = succ(succ(0)), Y = 0
% 
% greater_than(succ(succ(0)), 0)
% X = succ(0)