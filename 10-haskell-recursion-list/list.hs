-- Lecture Exercises

-- 2. Perfects

factors2 n = [x | x <- [1 .. n], n `mod` x == 0]
perfects2 n = [x | x <- [1.. n], sum(factors2 x) == 2*x]

scaler xs ys = sum([x * y | (x,y) <- (zip xs ys)])

-- Ex.1 - SquareSum
squaresum n = sum [ x ^2 | x <- take n [1..]]

-- Ex.2 - Grid
-- grid m x n
grid m n = [(x, y) | x <- [0 .. m], y <- [0 .. n]]

-- Ex.3 - Square
square n = [(x, y) | (x,y) <- grid n n, x /= y]

-- Ex.4 - Replicate
myreplicate n x = [ x | _ <- [1 .. n]]

replicate2 0 a = []
replicate2 x a = [a] ++ (replicate2 (x-1) a)

-- Ex.5 - Pythagoras
make_num_list n = [1 .. n]
pyths n = [(x, y, z) |  x <- make_num_list n, 
                        y <- make_num_list n, 
                        z <-make_num_list n, 
                        x^2 + y^2 == z ^2]

-- Ex.6 - Perfects
factors n = [ x | x <- [1..n], n `mod` x == 0]

-- init returns a list without the last element
-- init [1,2,3]
-- [1,2]
perfects n = [x | x <- [1 .. n], 
                x == sum(init (factors x))]

-- Ex.7 - 
myconcat = [(x, y) | x <- [1,2], y <- [3,4]]
myconcat2 = [[(x,y) | y <- [3,4]] | 
                                    x <- [1,2]]

-- Ex. 8
positions x xs = [i | (y, i) <- zip xs [0..], x == y]