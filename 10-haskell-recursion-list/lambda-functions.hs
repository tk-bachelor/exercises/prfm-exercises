nums :: [Int]
nums = [1,2,3,4,5,6,7,8,9,10]

-- Ex 1.

mapfilter f p xs = map f (filter p xs)

-- Ex 2.

all2 p xs = and (map p xs)

any2 p xs = or (map p xs)

takeWhile2 p [] = []
takeWhile2 p (x:xs) | p x = x : takeWhile2 p xs
                    | otherwise = []

dropWhile2 p [] = []
dropWhile2 p (x:xs) | p x = dropWhile2 p xs
                    | otherwise = (x:xs)

-- Ex 3.
map2 f = foldr (\x xs -> f x : xs) []
map3 f = foldl (\xs x -> xs ++ [f x]) []

filter2 p = foldr (\x xs -> if p x then (x:xs) else xs) []
filter3 p = foldl (\xs x -> if p x then xs ++ [x] else xs) []

-- Ex 4.
addIndex xs = zip xs (reverse [0 .. (length xs) -1])

dec2int :: [Int] -> Int
dec2int = foldl (\r x -> r * 10 + x) 0

-- Ex 5.
curry2 :: ((a, b) -> c) -> a -> b -> c
curry2 f = \x y -> f(x,y)

-- Ex 6.
unfold p h t x | p x = []
                | otherwise = h x : unfold p h t (t x)

int2bin = reverse . (unfold (== 0) (`mod` 2) (`div` 2))
chop8 = unfold null ( take 8) ( drop 8)

-- Ex 9.
altMap f1 f2 [] = []
altMap f1 f2 (x:xs) = f1 x : altMap f2 f1 xs

